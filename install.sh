#!/bin/bash
set -xv
current="$(dirname "$(realpath "$0")")"
dir="$HOME/.local/share/backgrounds"
mkdir -p "$dir"
ln -fsv "$current" "$dir/siaal-wallpapers"
